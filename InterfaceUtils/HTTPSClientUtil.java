/*******************************************************
 *Copyright (c) 2017 All Rights Reserved.
 *JDK版本： 1.8
 *公司名称：
 *命名空间：io.agilefast.common.utils
 *文件名：  HTTPSClientUtil 
 *版本号：  V1.0.0.0
 *创建人：  daixirui
 *电子邮箱：daixirui@live.com
 *创建时间：2019-03-27 18:08
 *描述：
 *
 *=====================================================
 *修改标记
 *修改时间：2019-03-27 18:08
 *修改人：  zhonghuijiang
 *版本号：  V1.0.0.0
 *描述：
 *
 /******************************************************/
package io.agilefast.common.utils;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

public class HTTPSClientUtil {
    private static final String DEFAULT_CHARSET = "UTF-8";

    public static String doPost(HTTPSClient httpsClient, String url, Map<String, String> paramHeader,
                                Map<String, String> paramBody) throws Exception {
        return doPost(httpsClient, url, paramHeader, paramBody, DEFAULT_CHARSET);
    }

    /**
     * Post请求
     * @param httpsClient Client对象
     * @param url 请求url地址
     * @param paramHeader 参数头
     * @param paramBody 参数内容
     * @param charset 编码设置
     * @return 返回结果
     * @throws Exception
     */
    public static String doPost(HTTPSClient httpsClient, String url, Map<String, String> paramHeader,
                                Map<String, String> paramBody, String charset) throws Exception {

        String result = null;
        HttpPost httpPost = new HttpPost(url);
        setHeader(httpPost, paramHeader);
        setBody(httpPost, paramBody, charset);


        HttpResponse response = httpsClient.execute(httpPost);
        if (response != null) {
            HttpEntity resEntity = response.getEntity();
            if (resEntity != null) {
                result = EntityUtils.toString(resEntity, charset);
            }
        }

        return result;
    }

    /**
     * Get请求
     * @param httpsClient Client对象
     * @param url         Url对象
     * @param paramHeader 请求头
     * @param paramBody   请求参数内容
     * @return 返回结果
     * @throws Exception
     */
    public static String doGet(HTTPSClient httpsClient, String url, Map<String, String> paramHeader,
                               Map<String, String> paramBody) throws Exception {
        return doGet(httpsClient, url, paramHeader, paramBody, DEFAULT_CHARSET);
    }

    public static String doGet(HTTPSClient httpsClient, String url, Map<String, String> paramHeader,
                               Map<String, String> paramBody, String charset) throws Exception {

        String result = null;
        HttpGet httpGet = new HttpGet(url);
        setHeader(httpGet, paramHeader);

        HttpResponse response = httpsClient.execute(httpGet);
        if (response != null) {
            HttpEntity resEntity = response.getEntity();
            if (resEntity != null) {
                result = EntityUtils.toString(resEntity, charset);
            }
        }

        return result;
    }

    private static void setHeader(HttpRequestBase request, Map<String, String> paramHeader) {
        // 设置Header
        if (paramHeader != null) {
            Set<String> keySet = paramHeader.keySet();
            for (String key : keySet) {
                request.addHeader(key, paramHeader.get(key));
            }
        }
    }

    /*
    *JSONObject json = new JSONObject();
            json.put("mobile", "13612345678");
            json.put("password","admin");
            String re= httpPostWithJSON("http://team.agiledev.com.cn:8080/WSApi/api/login",json.toString());
     */
    public static String httpPostWithJSON(String url,String par)  {

        HttpPost httpPost = new HttpPost(url);
        CloseableHttpClient client = HttpClients.createDefault();
        String respContent = null;

        StringEntity entity = new StringEntity(par,"utf-8");//解决中文乱码问题
        //System.out.println(entity.toString());
        entity.setContentEncoding("UTF-8");
        entity.setContentType("application/json");
        httpPost.setEntity(entity);

        try{
            HttpResponse resp = client.execute(httpPost);
            System.out.println(resp.getStatusLine().getStatusCode());
            if(resp.getStatusLine().getStatusCode() == 200) {
                HttpEntity he = resp.getEntity();
                respContent = EntityUtils.toString(he,"UTF-8");

            }

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return respContent;
    }

    private static void setBody(HttpPost httpPost, Map<String, String> paramBody, String charset) throws Exception {
        // 设置参数
        if (paramBody != null) {
            List<NameValuePair> list = new ArrayList<NameValuePair>();
            Set<String> keySet = paramBody.keySet();
            for (String key : keySet) {
                list.add(new BasicNameValuePair(key, paramBody.get(key)));
            }

            if (list.size() > 0) {
                UrlEncodedFormEntity entity = new UrlEncodedFormEntity(list, charset);
                httpPost.setEntity(entity);
            }
        }
    }
    /*
        private void test() throws Exception {

        HTTPSClient httpsClient = null;

        httpsClient = new HTTPSTrustClient().init();
        //httpsClient = new HTTPSCertifiedClient().init();

        String url = "http://localhost:8083/agilefast-api/api/loginv1";

        Map<String, String> paramHeader = new HashMap<>();
        paramHeader.put("Accept", "application/json");
        paramHeader.put("Content-Type", "application/json; charset=utf-8");
        Map<String, String> paramBody = new HashMap<>();
        paramBody.put("mobile", "13612345678");
        paramBody.put("password", "admin");
        String result = HTTPSClientUtil.doPost(httpsClient, url, paramHeader, paramBody);

        System.out.println(result);


    }
    */
}
