package io.agilefast.modules.medical.controller;

import io.agilefast.modules.medical.entity.ApiResultEntity;
import io.agilefast.modules.medical.entity.HoisQueueEntity;

import java.util.List;

/**
 * @Author: zhonghuijiang
 * @email: 928816561@qq.com
 * @Date: 2021/8/11 10:04
 */
public class ApiResult {

    private String msg;
    private List<ApiResultEntity> result;
    private String code;
    private String content;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<ApiResultEntity> getResult() {
        return result;
    }

    public void setResult(List<ApiResultEntity> result) {
        this.result = result;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
