package io.agilefast.utils;

import com.alibaba.fastjson.JSONObject;
import io.agilefast.modules.medical.controller.ApiResult;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * @Author: zhonghuijiang
 * @email: 928816561@qq.com
 * @Date: 2021/8/12
 */
public class InterfaceUtil {

    /***
     * post调用接口方法
     * @param path
     * @param data
     */
    public String PostinterfaceUtil(String path, String data) {
        //通过接口访问
        try{
            URL url = new URL(path);
            //打开和url之间的连接
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setDoInput(true);
            conn.setRequestProperty("Content-type", "application/json");
            conn.setRequestProperty("code", "ZHCX");
            PrintWriter out = null;
            BufferedReader in = null;
            String result = "";
            // 获取URLConnection对象对应的输出流
            out = new PrintWriter(new OutputStreamWriter(conn.getOutputStream(),"UTF-8"));
            // 发送请求参数
            out.print(data);
            // flush输出流的缓冲
            out.flush();
            // 定义BufferedReader输入流来读取URL的响应
            in = new BufferedReader(new InputStreamReader(conn.getInputStream(),"UTF-8"));
            String line;
            while ((line = in.readLine()) != null) {
                result += line;
            }
            return result;
        }catch (Exception e){
            e.printStackTrace();
            return "";
        }
    }

    /**
     * 分割名字
     * 姓名隐藏功能：2、3字名字隐藏第2位，4个字名字隐藏3、4个
     * @param name
     * @return
     */
    public String SplitName(String name){
        String [] arrName = name.split("");
        if (arrName.length==2){
            name = arrName[0]+"*";
        }
        if (arrName.length==3){
            name = arrName[0]+"*"+arrName[2];
        }
        if (arrName.length>3){
            name = arrName[0]+arrName[1]+"**";
        }
        return name;
    }

    /**
     * 解析json对象
     * @param result
     * @return
     */
    public ApiResult apiResult(String result){
        //解析json对象
        ApiResult apiResult = JSONObject.parseObject(result, ApiResult.class);
        return apiResult;
    }
}
