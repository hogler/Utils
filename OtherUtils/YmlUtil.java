/**
 * @Title: YmlUtil
 * @ProjectName agilefast-framework
 * @Package io.agilefast.common.utils
 * @Description: TODO
 * @author zhonghuijiang
 * @version V1.0.0
 * @Copyright: 2019  All rights reserved.
 * @date 2019/3/9 10:43
 */
package io.agilefast.common.utils;

import org.yaml.snakeyaml.Yaml;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class YmlUtil {

    /**
     * key:文件名索引
     * value:配置文件内容
     */
    private static Map<String, LinkedHashMap> ymls = new HashMap<>();

    /**
     * string:当前线程需要查询的文件名
     */
    private static ThreadLocal<String> nowFileName = new ThreadLocal<>();

    /**
     * 加载配置文件
     * @param fileName
     */
    public static void loadYml(String fileName) {
        nowFileName.set(fileName);
        if (!ymls.containsKey(fileName)) {
            ymls.put(fileName, new Yaml().loadAs(YmlUtil.class.getResourceAsStream("/" + fileName), LinkedHashMap.class));
        }
    }

    /**
     * @Author daixirui
     * @Description //TODO 获取值
     * @Date 11:08 2019/3/9
     * @Param [key] 对应配置值的key
     * @return java.lang.Object
     **/
    public static Object getValue(String key) throws Exception {
        // 首先将key进行拆分
        String[] keys = key.split("[.]");

        // 将配置文件进行复制
        Map ymlInfo = (Map) ymls.get(nowFileName.get()).clone();
        for (int i = 0; i < keys.length; i++) {
            Object value = ymlInfo.get(keys[i]);
            if (i < keys.length - 1) {
                ymlInfo = (Map) value;
            } else if (value == null) {
                throw new Exception("key不存在");
            } else {
                return value;
            }
        }
        throw new RuntimeException("不可能到这里的...");
    }

    /**
     * @Author daixirui
     * @Description //TODO 获取值
     * @Date 11:09 2019/3/9
     * @Param [fileName 文件名称, key 配置值的key]
     * @return java.lang.Object
     **/
    public static Object getValue(String fileName, String key) throws Exception {
        // 首先加载配置文件
        loadYml(fileName);
        return getValue(key);
    }


    public static void main(String[] args) throws Exception {
        System.out.println((getValue("application.yml", "server.port")));
    }
}

