/*******************************************************
 *Copyright (c) 2017 All Rights Reserved.
 *JDK版本： 1.8
 *公司名称：
 *命名空间：io.agilefast.common.utils
 *文件名：  TreeGridUtils 
 *版本号：  V1.0.0.0
 *创建人：  zhonghuijiang
 *电子邮箱：daixirui@live.com
 *创建时间：2018-11-06 10:13
 *描述：
 *
 *=====================================================
 *修改标记
 *修改时间：2021-11-06 10:13
 *修改人：  zhonghuijiang
 *版本号：  V1.0.0.0
 *描述：
 *
 /******************************************************/
package io.agilefast.common.utils;

import java.util.List;

public class TreeGridUtils {
    //设置属性

    private List<?> data;

    private String msg;

    private String code;

    public List<?> getData() {
        return data;
    }

    public void setData(List<?> data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
