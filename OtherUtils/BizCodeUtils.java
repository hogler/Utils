/**
 * @Title: BizCodeUtils
 * @ProjectName agilefast-framework
 * @Package io.agilefast.common.utils
 * @Description: TODO
 * @author zhonghuijiang
 * @version V1.0.0
 * @Copyright: 2020  All rights reserved.
 * @date 2020/12/1 12:01
 */
package io.agilefast.utils;

import io.agilefast.common.utils.DateUtils;
import io.agilefast.common.utils.MapUtils;
import io.agilefast.modules.sys.entity.SysBizCodeEntity;
import io.agilefast.modules.sys.entity.SysBizCodeRuleEntity;
import io.agilefast.modules.sys.service.SysBizCodeRuleService;
import io.agilefast.modules.sys.service.SysBizCodeService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Component
public class BizCodeUtils {
    @Autowired
    private SysBizCodeService sysBizCodeService;
    @Autowired
    private SysBizCodeRuleService sysBizCodeRuleService;

    /** 业务编号类型常量-常量(固定值) */
    public static final String BIZ_CODE_RULE_FIXED="常量";
    /** 业务编号类型常量-日期 */
    public static final String BIZ_CODE_RULE_DATE="日期";
    /** 业务编号类型常量-顺序号 */
    public static final String BIZ_CODE_RULE_SEQ="顺序号";

    /** 业务编号重置类型常量-不重置 */
    public static final String BIZ_CODE_REST_RULE_NOTREST="不重置";
    /** 业务编号重置类型常量-每日重置 */
    public static final String BIZ_CODE_REST_RULE_DAY="每日";
    /** 业务编号重置类型常量-每月重置 */
    public static final String BIZ_CODE_REST_RULE_MONTH="每月";
    /** 业务编号重置类型常量-每年重置 */
    public static final String BIZ_CODE_REST_RULE_YEAR="每年";

    /** 业务编号补齐方向常量-左 */
    public static final String BIZ_CODE_PADDING_SIDE_LEFT="左";
    /** 业务编号补齐方向常量-右 */
    public static final String BIZ_CODE_PADDING_SIDE_RIGHT="右";

    /**
     * 生成业务编码（默认不修改数据库）
     * @param bizCodeNo 业务编号
     * @return 返回生成的业务编码
     */
    public String buildCode(String bizCodeNo)
    {
        return this.buildCode(bizCodeNo,true);
    }

    /**
     * 生成业务编码
     * @param bizCodeNo 业务编号
     * @param isTest 是否测试(为true，只为测试显示，不更改数据库)
     * @return 返回生成的业务编码
     */
    public  String  buildCode(String bizCodeNo,boolean isTest)
    {
        SysBizCodeEntity sysBizCode=sysBizCodeService.queryBizCodeBybizCodeNo(bizCodeNo);
        Map<String, Object> params=new MapUtils();
        params.put("bizCodeId",sysBizCode.getBizCodeId());
        List<SysBizCodeRuleEntity> rules=sysBizCodeRuleService.queryListByBizCodeID(params);

        StringBuffer bufBizCode=new StringBuffer();
        for(SysBizCodeRuleEntity rule : rules)
        {
            if (rule.getRuleMode().equals(BIZ_CODE_RULE_FIXED))
            {
                //常量
                bufBizCode.append(rule.getRuleDelimiter());
                bufBizCode.append(rule.getRuleValue());
            }
            else if (rule.getRuleMode().equals(BIZ_CODE_RULE_DATE))
            {
                //日期
                bufBizCode.append(rule.getRuleDelimiter());
                bufBizCode.append(DateUtils.format(DateUtils.getCurrentDate(),rule.getRuleValue()));

            }
            else if (rule.getRuleMode().equals(BIZ_CODE_RULE_SEQ))
            {
                //顺序号
                bufBizCode.append(rule.getRuleDelimiter());

                boolean blIsRest=verifyIsRest(rule.getRestRule(),rule.getLastUpdateDate());
                if (blIsRest==true)
                {
                    //重置顺序号
                    rule.setCurrentNo(rule.getRuleValue());
                }
                int CurrentNo=Integer.parseInt(rule.getCurrentNo());
                CurrentNo++;
                //补齐宽度
                int intPaddingWide=Integer.parseInt(rule.getPaddingWide());
                if (String.valueOf(CurrentNo).length()<intPaddingWide)
                {
                    //顺序号字符串长度小于补齐宽度，则进行字符串补齐
                    if (rule.getPaddingSide().equals(BIZ_CODE_PADDING_SIDE_LEFT))
                    {
                        //左补齐
                        bufBizCode.append(StringUtils.leftPad(String.valueOf(CurrentNo), intPaddingWide, "0"));
                    }
                    else if (rule.getPaddingSide().equals(BIZ_CODE_PADDING_SIDE_RIGHT))
                    {
                        //右补齐
                        bufBizCode.append(StringUtils.rightPad(String.valueOf(CurrentNo), intPaddingWide, "0"));
                    }
                    else
                    {
                        //默认左补齐
                        bufBizCode.append(StringUtils.leftPad(String.valueOf(CurrentNo), intPaddingWide, "0"));
                    }
                }
                else
                {
                    //顺序号字符串长度大于等于补齐宽度，直接拼接
                    bufBizCode.append(String.valueOf(CurrentNo));
                }
                if (isTest==false)
                {
                    //非测试（正式生成顺序号）
                    rule.setCurrentNo(String.valueOf(CurrentNo));
                    rule.setLastUpdateDate(DateUtils.getCurrentDate());
                    sysBizCodeRuleService.updateById(rule);
                }
            }
        }
        return bufBizCode.toString();
    }

    public  boolean verifyIsRest(String restRule, Date lastUpdateDate)
    {
        if (restRule.equals(BIZ_CODE_REST_RULE_NOTREST))
        {
            return false;
        }
        else if  (restRule.equals(BIZ_CODE_REST_RULE_DAY))
        {
            String curDate=DateUtils.format(DateUtils.getCurrentDate(),DateUtils.DATE_PATTERN);
            String lastDate=DateUtils.format(lastUpdateDate,DateUtils.DATE_PATTERN);
            if (!curDate.equals(lastDate))
            {
                //不是同一天
                return true;
            }
            else
            {
                //同一天
                return false;
            }
        }
        else if  (restRule.equals(BIZ_CODE_REST_RULE_MONTH))
        {
            String curDate=DateUtils.format(DateUtils.getCurrentDate(),"yyyy-MM");
            String lastDate=DateUtils.format(lastUpdateDate,"yyyy-MM");
            if (!curDate.equals(lastDate))
            {
                //不是同一月
                return true;
            }
            else
            {
                //同一月
                return false;
            }
        }
        else if  (restRule.equals(BIZ_CODE_REST_RULE_YEAR))
        {
            String curDate=DateUtils.format(DateUtils.getCurrentDate(),"yyyy");
            String lastDate=DateUtils.format(lastUpdateDate,"yyyy");
            if (!curDate.equals(lastDate))
            {
                //不是同一年
                return true;
            }
            else
            {
                //同一年
                return false;
            }
        }
        else
        {
            return false;
        }
    }
}
